import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./views/Home";
import Favorites from "./views/Favorites";

function BasicExample() {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/favorites">Favorites</Link>
          </li>
        </ul>

        <hr />

        <Route exact path="/" component={Home} />
        <Route path="/favorites" component={Favorites} />
      </div>
    </Router>
  );
}

export default BasicExample;
